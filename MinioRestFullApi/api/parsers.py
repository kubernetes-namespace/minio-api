from email.policy import default
from secrets import choice
from flask_restplus import reqparse
from pkg_resources import require

pagination_parser = reqparse.RequestParser()

pagination_parser.add_argument('page', type=int, require=False, default=1, help="Page number")
pagination_parser.add_argument('item_per_page', type=int, require=False, default=2, choices=[5, 10,15], help='item per page')

