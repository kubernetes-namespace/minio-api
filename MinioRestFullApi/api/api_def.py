from flask_restplus import apidoc

api = apidoc(version=0.1, title="my minio s3 api", description='this API give access to how minio s3')

@api.error_handlers
def std_handler(e):
    return {'message': 'An unexpected error hast occured. Pleas contact'}
