from sqlite3 import dbapi2
from MinioRestFullApi.api.endpoints.buckets import Buckets


from MinioRestFullApi.database import db
from MinioRestFullApi.database.dtos import Bucket

def create_bucket(data):
    name = data.get('name')
    
    bucket = Bucket(name)
    db.add(bucket)

def read_bucket(data):
    name = data.get('name')
    bucket = Bucket(name)
    return db.get(bucket)

def delete_bucket(data):
    pass