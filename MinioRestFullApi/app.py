from flask import Flask, Blueprint
import settings
from api.api_def import api
from MinioRestFullApi.api.endpoints.buckets import namespace as bucketnamespace
from MinioRestFullApi.api.endpoints.clients import namespace as clientnamespace
from MinioRestFullApi.database.db import db

app = Flask(__name__)


def configure_app(app):
    # app.config['SERVER_NAME'] = settings.FLASK_SERVER_NAME
    app.config['SWAGGER_UI_DOC_EXPANSION'] = settings.SWAGGER_UI_DOC_EXPANSION
    app.config['RESTPLUS_VALIDATE'] = settings.RESTPLUS_VALIDATE
    app.config['RESTPLUS_MASK_SWAGGER'] = settings.RESTPLUS_MASK_SWAGGER
    app.config['SQLALCHEMY_DATABASE_URI'] = settings.SQLALCHEMY_DATABASE_URI
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = settings.SQLALCHEMY_TRACK_MODIFICATIONS
    

def init_app(app):
    configure_app(app)
    blueprint = Blueprint('api',__name__,url_prefix='/api')
    api.init_app(blueprint)
    api.add_namespace(bucketnamespace)
    api.add_namespace(clientnamespace)
    # api.add_namespace(objeckt)

    app.register_blueprint(blueprint)
    
    db.init_app(app)


def main():
    init_app(app)
    app.run(debug=settings.FLASK_DEBUG, threaded=settings.FLASK_THREADED)


if __name__ == "__main__":
    main()
