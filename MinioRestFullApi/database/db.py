from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

def reset():
    db.drop_all()
    db.create_all()

