from webbrowser import get
from fastapi import FastAPI
from app.authentication import router as auth_routes

from app import database


# from app import models
# from sqlalchemy.orm import session
# from app.database  import sessionLocal, engine

app = FastAPI()

# Create Database
@app.on_event("startup")
def startup_event():
    database.create_db_and_tables()

@app.on_event("shutdown") 
def shutdown_event():
    database.shutdown()



app.include_router(auth_routes.router)

@app.get("/")
def root():
    return {"hello": "Romeo"}