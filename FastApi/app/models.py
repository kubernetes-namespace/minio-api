from distutils.command.clean import clean
import email
from email.policy import default
from typing import Optional
from pydantic import EmailStr

#from sqlalchemy import String, Integer, Column, ForeignKey
#from sqlalchemy.orm import relationship
#from app.database import Base

from sqlmodel import SQLModel, Field
from enum import Enum

# Detail about sqlmodel 
# https://sqlmodel.tiangolo.com/tutorial/relationship-attributes/define-relationships-attributes/

class Roles(str, Enum):
    api= "api"
    user = "user"
    
class BaseUser(SQLModel):
    username: str
    is_active: bool = False
    role: Roles

class Client(BaseUser, table=True):
    client_id: Optional[str]= Field(default=None, primary_key=True)
    client_secret: Optional[str]= Field(default=None)
    
    # def __init__(self, client_id, client_secret):
    #     self.client_id = client_id
                        
    #     if client_secret is not None :
    #         self.client_secret = client_secret

class AdminUser(BaseUser, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    email: EmailStr
    hashed_password: str
    
class Bucket(SQLModel):
    id :  Optional[int] = Field(default=None, primary_key=True)
    name : str
    client_api: Optional[int]= Field(default=None, foreign_key='client.client_id')
    
    # def __init__(self, name, client_api):
    #     self.name = name
    #     if isinstance(client_api, str):
    #         self.client_api = client_api
    #     else:
    #         raise ValueError(f"unsupported str format: {client_api}")