import email
from unicodedata import name
from sqlmodel import Session
from FastApi.app import models

def create_user_admin(db:Session, user:models.AdminUser):
    hashed_password = user.hashed_password + "hashed"
    
    db_user_admin = models.AdminUser(
        email = user.email,
        username= user.username,
        role= user.role,
        hashed_password=hashed_password
    )
    
    db.add(db_user_admin)
    db.commit()
    db.refresh()
    
    return db_user_admin