from fastapi import APIRouter
from app import database, models
router = APIRouter()


@router.post("/users/register")
def register_user_admin(user:models.AdminUser):
    return [{"create user"}, {"user to create"}]

@router.get("/users/", tags=["users"])
async def read_users():
    return [{"username": "Rick"}, {"username": "Morty"}]


@router.get("/users/me", tags=["users"])
async def read_user_me():
    return {"username": "fakecurrentuser"}

@router.get("/users/{username}", tags=["users"])
async def read_user(username: str):
    return {"username": username}