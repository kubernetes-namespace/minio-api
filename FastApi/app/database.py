import os

from fastapi import Path

from sqlmodel import Session, SQLModel, create_engine
from dotenv import load_dotenv
load_dotenv()

# from sqlalchemy import create_engine
# from sqlalchemy.ext.declarative import declarative_base
# from sqlalchemy.orm import sessionmaker

engine = create_engine(
    os.environ.get("SQLALCHEMY_DATABASE_URI"),echo=True, future=True
)

# sessionLocal = sessionmaker(autocommit=False,autoflush=False, bind=engine)

# Base=declarative_base()

def get_db():
    with Session(engine) as session:
        yield session

def create_db_and_tables():
    SQLModel.metadata.create_all(engine)
    
def shutdown():
    cwd = Path.cwd().resolve()
    db_file = [file for file in os.listdir() if file.endswith(".db")][0]
    os.remove(os.path.join(cwd,db_file))
